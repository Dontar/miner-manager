import { Miner } from "./Miner";
import { Pool } from "./Pool";
import { Device } from "./Device";

export class Manager {
	private devices: Device[] = [];
	private miners: Miner[] = [];
	private pools: Pool[] = [];

	addMiner() {}
	delMiner() {}

	addPool() {}
	delPool() {}

	detectDevices() {}
}