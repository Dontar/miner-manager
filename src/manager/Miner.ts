import { Manager } from "./Manager";
import { Device } from "./Device";
import { Pool } from "./Pool";

export class Miner {
	constructor(private manager: Manager, private devices: Device[], private pool: Pool) { }

	start() {}
	stop() {}
}